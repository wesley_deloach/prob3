#! /usr/bin/env python
import os

sizes=[]
times=[]
sizesNewt=[]
timesNewt=[]
for k in range(5):
 Nx = 10 * 2**k
 modname = 'perf%d' % k
 options1 = ['-snes_type shell -snes_max_it 100000', '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname]
 os.system('./bin/ex5 '+' '.join(options1))
 perfmod = __import__(modname)
 sizes.append(Nx ** 2)
 times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])

 modname = 'perfNewt%d' % k
 options2 = ['-snes_type newtonls', '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname]
 os.system('./bin/ex5 '+' '.join(options2))
 perfmod = __import__(modname)
 sizesNewt.append(Nx ** 2)
 timesNewt.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
print zip(sizes, times)
print zip(sizesNewt, timesNewt)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
loglog(times, sizes, label='Simple Mixing')
loglog(timesNewt, sizesNewt, label='Newton')
title('SNES ex5')
ylabel('Precision (Problem Size)')
xlabel('Work (s)')
legend(loc='upper left')
show()

